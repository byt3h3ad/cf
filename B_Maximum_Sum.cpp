#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

void solve()
{
    int n, k;
    cin >> n >> k;
    vector<int> a(n);
    for (int &i : a)
    {
        cin >> i;
    }
    sort(a.begin(), a.end());
    vector<ll> sum(n + 1, 0);
    for (int i = 0; i < n; ++i)
    {
        sum[i + 1] = sum[i] + a[i];
    }
    ll ans = 0;
    for (int i = 0; i <= k; ++i)
    {
        ans = max(ans, sum[n - (k - i)] - sum[2 * i]);
    }
    cout << ans;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}