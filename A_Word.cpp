#include <bits/stdc++.h>
using namespace std;

void solve()
{
    string s;
    cin >> s;
    int u = 0, l = 0;
    for (int i = 0; i < s.length(); i++)
    {
        if (s[i] >= 'A' && s[i] <= 'Z')
            u++;
        else if (s[i] >= 'a' && s[i] <= 'z')
            l++;
    }
    if (u > l) {
        transform(s.begin(), s.end(), s.begin(), ::toupper);
    } else {
        transform(s.begin(), s.end(), s.begin(), ::tolower);
    }
    cout << s;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    // cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}