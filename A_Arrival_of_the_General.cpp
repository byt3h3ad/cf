#include <bits/stdc++.h>
using namespace std;

void solve()
{
    int n;
    cin >> n;
    int mn = 101, mx = 0, mn_i, mx_i;
    for (int i = 0; i < n; ++i)
    {
        int t;
        cin >> t;
        if (mn >= t)
        {
            mn = t;
            mn_i = i;
        }
        if (mx < t)
        {
            mx = t;
            mx_i = i;
        }
    }
    cout << mn_i << " " << mx_i;
    // int res = max_i;
    // if(max_i > min_i) res--;
    // res += n - min_i - 1;
    // cout << res << endl;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    // cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}