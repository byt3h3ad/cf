#include <bits/stdc++.h>
using namespace std;

bool unique(int n)
{
    int mp[10] = {0};
    while (n)
    {
        int digit = n % 10;
        n /= 10;
        if (mp[digit]++)
            return 0;
    }
    return 1;
}

void solve()
{
    int year;
    cin >> year;
    for (int i = year + 1;; ++i)
    {
        if (unique(i))
        {
            cout << i;
            return;
        }
    }
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    // cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}