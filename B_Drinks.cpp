#include <bits/stdc++.h>
using namespace std;

void solve()
{
    double n, sum = 0;
    cin >> n;
    for (int i = 0; i < n; ++i)
    {
        double x;
        cin >> x;
        sum += x;
    }
    printf("%.10lf\n", sum / n);
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    // cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}