#include <bits/stdc++.h>
using namespace std;

void solve()
{
    int n;
    cin >> n;
    int ans = -1, mx = INT_MIN;
    for (int i = 0; i < n; ++i)
    {
        int a, b;
        cin >> a >> b;
        if (b > mx && a <= 10)
        {
            ans = i + 1;
            mx = b;
        }
    }
    cout << ans;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}