#include <bits/stdc++.h>
using namespace std;

void solve()
{
    int n, k;
    cin >> n >> k;
    vector<int> dp(n + 1, 0);
    dp[0] = 1;

    for (int i = 0; i < k; i++)
    {
        for (int j = n; j >= 2; j--)
        {
            if (j - (1 << i) >= 0)
            {
                dp[j] += dp[j - (1 << i)];
            }
        }
    }

    cout << dp[n];
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}