#include <vector>
#include <list>
#include <map>
#include <set>
#include <deque>
#include <queue>
#include <stack>
#include <bitset>
#include <algorithm>
#include <functional>
#include <numeric>
#include <utility>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include <cctype>
#include <string>
#include <cstring>
#include <cstdio>
#include <ctime>
#include <climits>
#include <complex>
using namespace std;

typedef long long ll;

void solve()
{
    int x[200], y[200], n, s = 0;
    cin >> n;
    for (int i = 1; i <= n; i++)
    {
        cin >> x[i] >> y[i];
    }
    for (int i = 1; i <= n; i++)
    {
        int l = 0, r = 0, u = 0, d = 0;
        for (int j = 1; j <= n; j++)
        {
            if (i != j)
            {
                if (x[i] == x[j])
                {
                    if (y[i] < y[j])
                        r++;
                    if (y[i] > y[j])
                        l++;
                }
                if (y[i] == y[j])
                {
                    if (x[i] < x[j])
                        u++;
                    if (x[i] > x[j])
                        d++;
                }
            }
        }
        if (l && r && u && d)
            s++;
    }
    cout << s;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    // cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}