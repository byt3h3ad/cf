#include <bits/stdc++.h>
using namespace std;

void solve()
{
    int n, m, k, h;
    cin >> n >> m >> k >> h;
    vector<int> height(n);
    for (auto &i : height) {
        cin >> i;
    }
    int count = 0;
    for (auto i : height) {
        int diff = abs(h - i);
        if (diff % k == 0) {
            if (diff/k < m && (diff / k != 0)) {
                count++;
            }
        }
    }
    cout << count;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}