#include <bits/stdc++.h>
using namespace std;

void solve()
{
    vector<vector<int>> v(10, vector<int>(10, 0));
    for (int i = 1; i <= 3; ++i)
    {
        for (int j = 1; j <= 3; ++j)
        {
            cin >> v[i][j];
        }
    }
    for (int i = 1; i <= 3; i++)
    {
        for (int j = 1; j <= 3; j++)
        {
            int cnt = v[i][j] + v[i - 1][j] + v[i + 1][j] + v[i][j - 1] + v[i][j + 1];
            if (cnt % 2 == 1)
            {
                cout << "0";
            }
            else
            {
                cout << "1";
            }
        }
        cout << "\n";
    }
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    // cin >> t;
    while (t--)
    {
        solve();
    }
    return 0;
}