#include <bits/stdc++.h>
using namespace std;

void solve()
{
    int n;
    cin >> n;
    if (n & 1)
    {
        cout << -1;
    }
    else
    {
        for (int i = 1; i <= n / 2; ++i)
        {
            cout << 2 * i << " " << 2 * i - 1 << " ";
        }
    }
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    // cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}