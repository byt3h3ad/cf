// https://codeforces.com/problemset/problem/248/A

#include <bits/stdc++.h>
using namespace std;

void solve()
{
    int n, ls = 0, rs = 0;
    cin >> n;
    for (int i = 0; i < n; ++i)
    {
        int l, r;
        cin >> l >> r;
        if (l)
            ls++;
        if (r)
            rs++;
    }
    if (ls * 2 > n)
        ls = n - ls;
    if (rs * 2 > n)
        rs = n - rs;
    cout << ls + rs;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    // cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}