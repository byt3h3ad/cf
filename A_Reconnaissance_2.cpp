#include <bits/stdc++.h>
using namespace std;

void solve()
{
    int n;
    cin >> n;
    vector<int> v(n + 1);
    for (int i = 0; i < n; ++i)
        cin >> v[i];
    v[n] = v[0];
    int idx = 0;
    for (int i = 1; i < n; ++i)
    {
        if (abs(v[i + 1] - v[i]) < abs(v[idx + 1] - v[idx]))
        {
            idx = i;
        }
    }
    cout << (idx + 1) << " " << (idx + 1) % n + 1;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    // cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}