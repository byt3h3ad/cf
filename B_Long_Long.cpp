#include <bits/stdc++.h>
using namespace std;

void solve()
{
    int n;
    cin >> n;
    vector<int> a(n);
    for (int &i : a)
    {
        cin >> i;
    }
    bool flag = false;
    long long sum = 0, count = 0;
    for (auto i : a)
    {
        sum += abs(i);
        if (i < 0 && !flag)
        {
            flag = true;
            count++;
        }
        else if (i > 0)
        {
            flag = false;
        }
    }
    cout << sum << " " << count;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}