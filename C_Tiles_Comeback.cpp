#include <bits/stdc++.h>
using namespace std;

void solve()
{
    int n, k;
    cin >> n >> k;
    vector<int> a(2e5 + 10);
    for (int i = 0; i < n; ++i)
    {
        cin >> a[i];
    }
    vector<int> v(2e5 + 10, 0);
    bool flag = true;
    for (int i = 0; i < n; ++i)
    {
        if (v[a[i]] && i - v[a[i]] != k)
        {
            flag = false;
            break;
        }
        v[a[i]] = i;
    }
    cout << (flag ? "YES" : "NO");
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}