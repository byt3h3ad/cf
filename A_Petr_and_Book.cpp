#include <bits/stdc++.h>
using namespace std;

void solve()
{
    int n;
    cin >> n;
    vector<int> v(n + 1);
    for (int i = 1; i <= 7; ++i)
    {
        cin >> v[i];
    }
    while(1) {
        for (int i = 1; i <= 7; ++i) {
            if (n <= v[i]) {
                cout << i;
                return;
            } else {
                n -= v[i];
            }
        }
    }
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    // cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}