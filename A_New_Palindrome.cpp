#include <bits/stdc++.h>
using namespace std;

void solve()
{
    string s; cin >> s;
    string t = s.substr(0, s.length()/2);
    sort(t.begin(), t.end());
    cout << (t[0] == t.back() ? "NO" : "YES");
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}