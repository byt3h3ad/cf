#include <bits/stdc++.h>
using namespace std;

void solve()
{
    int x, y;
    cin >> x >> y;
    vector<bool> prime(2500, false);
    vector<int> mp;
    for (int i = 2; i * i < 2500; ++i)
    {
        if (!prime[i])
        {
            mp.push_back(i);
            for (int j = i * 2; j < 2500; j += i)
                prime[j] = true;
        }
    }
    for (int i = 0; i < mp.size(); ++i)
    {
        if (mp[i] == x)
        {
            if (mp[i + 1] == y)
                cout << "YES";
            else
                cout << "NO";
        }
    }
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    // cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}