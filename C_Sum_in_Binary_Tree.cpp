#include <bits/stdc++.h>
using namespace std;

void solve()
{
    long long n, sum = 0;
    cin >> n;
    while (n >= 1)
    {
        sum += n;
        n /= 2;
    }
    cout << sum;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}