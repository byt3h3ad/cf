#include <bits/stdc++.h>
using namespace std;

void solve()
{
    int n;
    cin >> n;
    string s;
    cin >> s;
    string a = "";
    char c = s[0];
    for (int i = 1; i < n; ++i)
    {
        if (c == s[i])
        {
            a += s[i];
            c = s[i+1];
            i++;
        }
    }
    cout << a;
}

int main()
{
    int t = 1;
    cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}