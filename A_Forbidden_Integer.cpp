#include <bits/stdc++.h>
using namespace std;

void solve()
{
    int n, k, x;
    cin >> n >> k >> x;
    vector<int> ans;
    if (x != 1)
    {
        ans = vector<int>(n, 1);
    }
    else
    {
        if (n == 1)
        {
            cout << "NO";
            return;
        }
        else if (k == 1 || n == 1)
        {
            cout << "NO";
            return;
        }
        else if (k == 2)
        {
            if (n & 1)
            {
                cout << "NO";
                return;
            }
            else
            {
                ans = vector<int>(n / 2, 2);
            }
        }
        else
        {
            if (n & 1)
            {
                ans = vector<int>((n - 3) / 2, 2);
                ans.emplace_back(3);
            }
            else
            {
                ans = vector<int>(n / 2, 2);
            }
        }
    }
    cout << "YES"
         << "\n";
    cout << ans.size() << "\n";
    for (int i : ans)
    {
        cout << i << " ";
    }
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}