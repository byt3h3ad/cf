#include <bits/stdc++.h>
using namespace std;

void solve()
{
    int k, l, m, n, d, ans = 0;
    cin >> k >> l >> m >> n >> d;
    for (int i = 1; i <= d; ++i)
    {
        ans += (i % k == 0 || i % l == 0 || i % m == 0 || i % n == 0);
    }
    cout << ans;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    // cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}