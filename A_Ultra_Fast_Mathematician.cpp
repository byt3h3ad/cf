#include <bits/stdc++.h>
using namespace std;

void solve()
{
    string x, y, z;
    cin >> x >> y;
    for (int i = 0; i < x.length(); ++i)
    {
        if (x[i] == y[i])
            cout << "0";
        else
            cout << "1";
    }
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    // cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}