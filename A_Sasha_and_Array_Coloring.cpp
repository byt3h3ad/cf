#include <bits/stdc++.h>
using namespace std;

void solve()
{
    int n;
    cin >> n;
    vector<int> a(n);
    for (int &i : a)
        cin >> i;
    sort(a.begin(), a.end());
    int l = 0, r = n - 1;
    long long sum = 0;
    while (l < r)
    {
        sum += a[r] - a[l];
        l++, r--;
    }
    cout << sum;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}