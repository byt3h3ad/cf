#include <iostream>
#include <cstdio>
#include <cstring>
#include <map>
#include <vector>
#include <string>
#include <queue>
#include <cmath>
#include <algorithm>
using namespace std;
typedef long long ll;
const int MOD = 1000000007;

void solve()
{
    string s;
    cin >> s;
    vector<int> ans;
    for (int i = 0; i < s.size(); i += 2)
    {
        ans.push_back(s[i] - '0');
    }
    sort(ans.begin(), ans.end());
    cout << ans[0];
    for (int i = 1; i < ans.size(); i++)
    {
        cout << "+" << ans[i];
    }
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    // cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}