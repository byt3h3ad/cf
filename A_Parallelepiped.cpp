#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

void solve()
{
    int a, b, c;
    cin >> a >> b >> c;
    ll r = (ll)a * b * c;
    r = (ll)(sqrt(r) + 0.5);
    int ans = 0;
    ans = r / a + r / b + r / c;
    ans *= 4;
    cout << ans;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    // cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}