#include <bits/stdc++.h>
using namespace std;

void solve()
{
    long long x;
    cin >> x;
    int ans = 0;
    while (x)
    {
        int digit = x % 10;
        if (digit == 4 || digit == 7)
            ans++;
        x /= 10;
    }
    cout << ((ans == 4 || ans == 7) ? "YES" : "NO");
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    // cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}