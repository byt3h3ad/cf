from sys import stdin
input = lambda: stdin.buffer.readline().decode().strip()
cin = lambda: map(int, input().split(' '))

for _ in range(int(input())):
    _, k = cin()
    a = list(cin())
    if k in a: print('YES')
    else: print('NO')