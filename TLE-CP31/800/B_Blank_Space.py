from sys import stdin
input = lambda: stdin.buffer.readline().decode().strip()
cin = lambda t=int : map(t, input().split(' '))

for _ in range(int(input())):
    n, a = int(input()), list(cin())
    ans, k = 0, 0
    for i in a:
        if i == 0: k += 1
        else:
            ans = max(k, ans)
            k = 0
    print(max(k, ans))