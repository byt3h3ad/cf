from sys import stdin
input = lambda: stdin.buffer.readline().decode().strip()
cin = lambda t=int : map(t, input().split(' '))

for _ in range(int(input())):
    x, k = cin()
    if x % k == 0:
        print(2)
        print(1, x - 1)
    else:
        print(1)
        print(x)
        
