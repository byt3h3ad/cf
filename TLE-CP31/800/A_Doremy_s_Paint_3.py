from sys import stdin
from collections import Counter
input = lambda: stdin.buffer.readline().decode().strip()

def solve():
    _ = input()
    b = list(Counter(map(int, input().split(' '))).values())
    if len(b) > 2:
        print("NO")
    elif abs(b[0] - b[-1]) <= 1:
        print("YES")
    else:
        print("NO")

for _ in range(int(input())):
    solve()