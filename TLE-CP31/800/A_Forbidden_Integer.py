from sys import stdin
input = lambda: stdin.buffer.readline().decode().strip()
cin = lambda t=int : map(t, input().split(' '))

for _ in range(int(input())):
    n, k, x = cin()
    if x != 1:
        res = [1] * n
        print("YES")
        print(len(res))
        print(*res)
    else:
        if n == 1: print("NO")
        elif k == 1 or n == 1: print("NO")
        elif k == 2:
            if n & 1: print("NO")
            else:
                res = [2] * (n//2)
                print("YES")
                print(len(res))
                print(*res)
        else:
            if (n & 1):
                res = [2] * ((n - 3)//2)
                res.append(3)
                print("YES")
                print(len(res))
                print(*res)
            else:
                res = [2] * (n//2)
                print("YES")
                print(len(res))
                print(*res)
        
