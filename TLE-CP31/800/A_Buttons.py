from sys import stdin
input = lambda: stdin.buffer.readline().decode().strip()
cin = lambda t=int : map(t, input().split(' '))

for _ in range(int(input())):
    a, b, c = cin()
    if c & 1:
        if b > a:
            print("Second")
        else:
            print("First")
    else:
        if b >= a:
            print("Second")
        else:
            print("First")