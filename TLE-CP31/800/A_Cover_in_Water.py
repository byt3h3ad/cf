from sys import stdin
input = lambda: stdin.buffer.readline().decode().strip()

for _ in range(int(input())):
    _ = input()
    a = input()
    if "..." in a:
        print(2)
    else:
        print(a.count('.'))