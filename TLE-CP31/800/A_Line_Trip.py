from sys import stdin
input = lambda: stdin.buffer.readline().decode().strip()

for _ in range(int(input())):
    n, x = map(int, input().split(' '))
    a = list(map(int, input().split(' ')))
    ans = max(a[0], 2*(x - a[-1]))
    for i in range(n-1):
        ans = max(ans, a[i+1] - a[i])
    print(max(ans, 2*(x - a[n-1])))