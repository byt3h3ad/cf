from sys import stdin
input = lambda: stdin.buffer.readline().decode().strip()

for _ in range(int(input())):
    _ = input()
    a = list(map(int, input().split(' ')))
    if a[0] == 1:
        print("YES")
    else:
        print("NO")