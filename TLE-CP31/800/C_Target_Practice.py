from sys import stdin
input = lambda: stdin.buffer.readline().decode().strip()
cin = lambda t=int : map(t, input().split(' '))

def solve():
    grid = []
    for _ in range(10):
        grid.append(input())
    ans = 0
    for i in range(10):
        for j in range(10):
            if grid[i][j] == 'X':
                ans += min(i+1, j+1, 10-i, 10-j)
    print(ans)

for _ in range(int(input())):
    solve()