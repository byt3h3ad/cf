# https://codeforces.com/contest/1859/submission/218502337
from sys import stdin
input = lambda: stdin.buffer.readline().decode().strip()
cin = lambda t=int : map(t, input().split(' '))

for _ in range(int(input())):
    _ = input()
    a = list(cin())
    mn = 10**5 + 1
    for i in a:
        if i < 0: i = -1*i
        mn = min(mn, i)
    print(mn)
