from sys import stdin
input = lambda: stdin.buffer.readline().decode().strip()

for _ in range(int(input())):
    n = int(input())
    if n % 3 == 0:
        print("Second")
    else:
        print("First")