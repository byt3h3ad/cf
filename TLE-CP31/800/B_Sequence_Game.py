from sys import stdin
input = lambda: stdin.buffer.readline().decode().strip()
cin = lambda t=int : map(t, input().split(' '))

def solve():
    n, a = int(input()), list(cin())
    b = []
    for i in range(n):
        if i and b[-1] > a[i]:
            b.append(1)
        b.append(a[i])
    print(len(b))
    print(*b)
for _ in range(int(input())):
    solve()