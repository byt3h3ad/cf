from sys import stdin
input = lambda: stdin.buffer.readline().decode().strip()
cin = lambda t=int : map(t, input().split(' '))

for _ in range(int(input())):
    n, a = int(input()), list(cin())
    k = a.count(-1)
    b = min(k, n//2)
    ans = k - b
    if b & 1: print(ans + 1)
    else: print(ans)