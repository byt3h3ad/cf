from sys import stdin
input = lambda: stdin.buffer.readline().decode().strip()
cin = lambda t=int : map(t, input().split(' '))

for _ in range(int(input())):
    n, a = int(input()), list(cin())
    b = [0] * n
    for i in range(n):
        b[i] = n + 1 - a[i]
    print(*b)