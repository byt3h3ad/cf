from sys import stdin
input = lambda: stdin.buffer.readline().decode().strip()
cin = lambda t=int : map(t, input().split(' '))

def solve():
    _, _ = cin()
    x, s = input(), input()
    for i in range(6):
        if s in x:
            print(i)
            return
        x += x
    print(-1)

for _ in range(int(input())):
    solve()