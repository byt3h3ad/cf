from sys import stdin
input = lambda: stdin.buffer.readline().decode().strip()
cin = lambda t=int : map(t, input().split(' '))

def solve():
    _ = input()
    a = list(cin())
    print(sum(a)*-1)

for _ in range(int(input())):
    solve()