// https://codeforces.com/problemset/problem/155/A

#include <bits/stdc++.h>
using namespace std;

void solve()
{
    int n, mx, mn, cnt = 0;
    cin >> n >> mx;
    mn = mx;
    for (int i = 1; i < n; ++i)
    {
        int x;
        cin >> x;
        if (x > mx)
        {
            mx = x;
            cnt++;
        }
        else if (x < mn)
        {
            mn = x;
            cnt++;
        }
    }
    cout << cnt;
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    // cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}