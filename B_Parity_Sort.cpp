#include <bits/stdc++.h>
using namespace std;

void solve()
{
    int n;
    cin >> n;
    vector<int> a(n);
    for (int &i : a)
    {
        cin >> i;
    }
    vector<int> b(a);
    sort(a.begin(), a.end());
    for (int i = 0; i < n; ++i)
    {
        if ((a[i] & 1) != (b[i] & 1))
        {
            cout << "NO";
            return;
        }
    }
    cout << "YES";
}

int main()
{
    ios::sync_with_stdio(false);
    cin.tie(0);
    int t = 1;
    cin >> t;
    while (t--)
    {
        solve(), cout << "\n";
    }
    return 0;
}